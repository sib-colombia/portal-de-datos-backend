#!/usr/bin/python
# coding: utf-8
'''
* API para servicios de backend para portales de datos del SiB Colombia sobre flask
  autor: el3ctron
  fecha de creación: mayo 2018 '''

'''
*funciones
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
** imports
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'''

from flask import Flask, request, jsonify, send_from_directory
from flask_cors import CORS
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from download import generate_download_job
from libs import build_query, build_query2, count_build_query, formatUrlSearch
import os
import sys

sys.setrecursionlimit(80)


def create_app():
    from download import rq
    app = Flask(__name__)
    rq.init_app(app)
    return app


application = create_app()
CORS(application)
es = Elasticsearch([os.environ['ELASTIC_SEARCH_SERVER']])
elastic_index = os.environ['ELASTIC_SEARCH_INDEX']


@application.route("/")
def hello():
    resp = {
        "API": "Api Datos Búsquedas (Elasticsearch) para datos del SIB",
        "Version": "2.0"
    }
    return jsonify(resp)


#TODO: Calcular el tiempo de la descarga para mostrar al usuario.
#TODO: Validar estado del worker de descargas.
#Metodo para generar una nueva descarga desde el portal de datos
@application.route("/api/newdownload", methods=["GET"])
def new_download():
    print(request)
    currentUrl=formatUrlSearch(request)
    job = generate_download_job.queue(request.args, request.args["email"], currentUrl)
    print("La respuesta es: ", job)
    resp = {
        "response": "Tarea de descarga programada",
        "status": "ok"
    }
    return jsonify(resp)


@application.route("/api/download/<key>", methods=["GET"])
def download(key):
    return send_from_directory('', key)


@application.route("/api/search", methods=["GET"])
def api_search():
    print(request.args)
    query = build_query(request)
    match = {
        "query": {
            "query_string": {
                "query": query
            }
        }
    }
    if "page" in request.args:
        match["from"] = request.args["page"]
        match["size"] = 20

    # print (match)
    resp = es.search(index=elastic_index, body=match)
    # print ("####################################")

    return jsonify(resp)


@application.route("/api/count", methods=["GET"])
def api_count():
    print(request.args)
    if len(request.args) > 0:
        query = build_query(request)
        match = {
            "query": {
                "query_string": {
                    "query": query
                }
            }
        }
    else:
        match = {
        }

    print(match)
    resp = es.count(index=elastic_index, body=match)

    return jsonify(resp)


@application.route("/api/agg", methods=["GET"])
def api_search_aggs():
    print(request.args)

    condicion = []
    agg = {}
    query = build_query(request)
    c_query = count_build_query(request)

    for v in request.args.getlist("agg"):
        print(v)
        agg[v] = {"terms": {"field": v},
                  "aggs":
                      {
                          "platform": {"top_hits": {"size": 1}}
                      }
                  }
    if c_query > 0:
        match = {
            "query": {
                "query_string": {
                    "query": query
                }
            },
            "aggs": agg
        }
    else:
        match = {
            "aggs": agg
        }
    if "page" in request.args:
        match["from"] = request.args["page"]
        match["size"] = 20
    print(match)
    print("####################################")

    resp = es.search(index=elastic_index, body=match)
    return jsonify(resp)


@application.route("/api/agg2", methods=["GET"])
def api_search_aggs2():
    print("----------------------###---------------------->")
    print(request.args)

    s = Search(using=es, index=elastic_index)

    s = build_query2(request.args, s)
    s = s[0:0]
    l = False
    print("-------------------------------------->")
    print(request.args)
    # print(request.args["limit"])
    if "limit" in request.args:
        l = request.args["limit"]
    if not l:
        l = 20

    for v in request.args.getlist("agg"):
        s.aggs.bucket(v, 'terms', size=l, field=v).metric('hits', 'top_hits', size=1)
        s.aggs.bucket(v + "_count", 'value_count', field=v)
        # .metric('stats', 'stats', field=v)

    print(s.to_dict())

    r = s.execute()
    '''
    print(r)

    print(r.success())
    # True

    print(r.took)
    # 12
    '''
    d = r.to_dict()
    # print("Cantidad de recursos encontrados", r.hits.total, "l=", len(d["aggregations"]["gbifId"]["buckets"]))

    '''
    agg={}
    c_query = count_build_query(request)

    for v in request.args.getlist("agg"):
        print (v)
        agg[v] = {"terms": {"field": v }, 
              "aggs": 
                  {
                    "platform": {"top_hits": {"size": 1}}  
                  }
              }

    if "page" in request.args:
        match["from"] = request.args["page"]
        match["size"] = 20
    print (match)
    print ("####################################")

    resp = es.search(index=elastic_index, body=match)
    '''

    return jsonify(r.to_dict())


@application.route("/api/auto", methods=["GET"])
def api_search_auto():
    match = {}
    query = build_query(request)
    if request.args["valor"] != "":
        print(request.args["valor"])
        match = {
            "suggest": {
                request.args["campo"] + "S": {
                    "text": request.args["valor"],
                    "completion": {
                        "field": request.args["campo"] + "S",
                        "skip_duplicates": True,
                        "size": 10
                    }
                }
            }
        }
        resp = es.search(index=elastic_index, body=match)
        print("response :" + str(resp))
        return jsonify(resp)
    else:
        match = {
            "size": 0,
            "aggs": {
                "lista": {
                    "cardinality": {
                        "field": request.args["campo"]
                    }
                }
            }

        }
        print("Opciones")
        print(match)
        resp = es.search(index=elastic_index, body=match)
        print(resp)
        '''
        suggest	
        geo_paramoS	
          0	
          length	2
          offset	0
          options	
            0	
              _id	"03ccba07-172f-4c38-a08d-e9827a5e5500"
              _index	"f-sib"
              _score	1
              _source	{…}
              _type	"register"
              text	"Sierra Nevada de Santa Marta "        
        '''
        return jsonify(resp)


@application.route("/api/stats", methods=["GET"])
def api_stats():
    ## por qué no lista parámetros repetidos?
    ##### Los parámetros repetidos se consultan como se muestra en el metodo: build_query
    r = {}

    query = build_query(request)
    match = {
        "query": {
            "query_string": {
                "query": query
            }
        },
        "size": 0,
        "aggs": {
            "recursos": {
                "cardinality": {
                    "field": "gbifId"
                }
            },
            "publicadores": {
                "cardinality": {
                    "field": "organizationId"
                }
            },
            "especies": {
                "cardinality": {
                    "field": "scientificName"
                }
            }
        }
    }
    print(match)
    resp = es.search(index=elastic_index, body=match, size=0)
    print(resp)

    query = build_query(request, "hasLocation: true")
    match = {
        "query": {
            "query_string": {
                "query": query
            }
        },
        "size": 0,
    }

    print(match)
    resp2 = es.search(index=elastic_index, body=match, size=0)
    print(resp2)

    r[0] = {"name": "REGISTROS", "value": resp["hits"]["total"]}
    r[1] = {"name": "RECURSOS", "value": resp["aggregations"]["recursos"]["value"]}
    r[2] = {"name": "PUBLICADORES", "value": resp["aggregations"]["publicadores"]["value"]}
    r[3] = {"name": "CON COORDENADAS", "value": resp2["hits"]["total"]}
    r[4] = {"name": "ESPECIES", "value": resp["aggregations"]["especies"]["value"]}

    return jsonify(r)


@application.route("/api/map", methods=["GET"])
def api_map():
    query = build_query(request)

    ''',
    "aggs": {"platform": {"top_hits": {"size": 1}}
    }'''

    match = {
        "size": 0,
        "_source": {"excludes": []},
        "aggs": {"filter_agg": {
            "filter": {
                "geo_bounding_box": {"location": {
                    "top_left": {"lat": 50.475775, "lon": -100.549805},
                    "bottom_right": {"lat": -50.070645, "lon": -50.954105}
                }}},
            "aggs": {"2": {"geohash_grid": {"field": "location", "precision": 4},
                           "aggs": {"3": {"geo_centroid": {"field": "location"}
                                          }}
                           }
                     }
        }},

        "query": {
            "query_string": {
                "query": query
            }
        }
    }
    '''{
      "query_string": {
              "query": query
          }
    }'''

    print(match)

    resp = es.search(index=elastic_index, body=match)
    return jsonify(resp)


if __name__ == "__main__":
    application.run(debug=True, port=os.environ['API_BUSQUEDAS_PORT'], host="0.0.0.0")