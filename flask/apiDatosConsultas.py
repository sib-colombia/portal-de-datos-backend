#!/usr/bin/python
# coding: utf-8
'''
* API para servicios de backend para portales de datos del SiB Colombia sobre flask
  autor: el3ctron
  fecha de creación: mayo 2018 '''

'''
*funciones
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
** imports
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'''

import os

from flask import Flask, request, jsonify, send_from_directory
import os
from flask_cors import CORS
import pymongo

url_mongo = os.environ['MONGO_SERVER']
database_mongo = os.environ['MONGO_DATA_BASE']

application = Flask(__name__)
CORS(application)


@application.route("/")
def hello():
    resp = {
        "API": "Api Datos Consultas (Mongo) para datos del SIB",
        "Version": "2.0"
    }
    return jsonify(resp)


def get_occurrence(key):
    try:
        client = pymongo.MongoClient(url_mongo)
        db = client[database_mongo]
        coll = db['occurrence']
        occurrence = coll.find_one({'occurrenceID': key})
        if occurrence is not None:
            occurrence.pop('_id')

        result = {}
        result["dataset"] = get_dataset(occurrence["gbifId"])
        result["organization"] = get_provider(occurrence["organizationId"])
        result["occurrence"] = occurrence
        return result
    except Exception as e:
        print('Error get_occurrence: ', e)
        return None


@application.route("/api/occurrence/<key>", methods=["GET"])
def api_occurrence(key):
    print('Query api_occurrence: key ', key)
    return_json = get_occurrence(key)
    if return_json is None:
        abort(404)
    else:
        return jsonify(return_json)
    print("####################################")


def get_dataset(gbifId):
    try:
        client = pymongo.MongoClient(url_mongo)
        db = client[database_mongo]
        coll = db['dataset']
        result = coll.find_one({'gbifId': gbifId})
        if result is not None:
            result.pop('_id')
        result["organization"] = get_provider(result["organizationId"])
        return result
    except Exception as e:
        print('Error get_dataset: ', e)
        return None


@application.route("/api/dataset/<gbifId>", methods=["GET"])
def api_dataset(gbifId):
    print('Query api_dataset: gbifId ', gbifId)
    return_json = get_dataset(gbifId)
    if return_json is None:
        abort(404)
    else:
        return jsonify(return_json)
    print("####################################")


def get_provider(provider):
    try:
        client = pymongo.MongoClient(url_mongo)
        db = client[database_mongo]
        coll = db['provider']
        result = coll.find_one({'providerId': provider})
        if result is not None:
            result.pop('_id')
        return result
    except Exception as e:
        print('Error get_provider: ', e)
        return None


@application.route("/api/provider/<provider>", methods=["GET"])
def api_provider(provider):
    print('Query api_provider: providerId ', provider)
    return_json = get_provider(provider)
    if return_json is None:
        abort(404)
    else:
        return jsonify(return_json)
    print("####################################")


if __name__ == "__main__":
    application.run(debug=True, port=os.environ['API_CONSULTAS_PORT'], host="0.0.0.0")