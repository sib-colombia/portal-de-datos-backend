from flask_rq2 import RQ
import zipfile
import os
import unicodecsv as csv
from elasticsearch import Elasticsearch
import smtplib
import csv
import glob
import traceback
from libs import build_query2
from libs import formatToAscii
from elasticsearch_dsl import Search
import pandas as pd
import hashlib as hasher


es = Elasticsearch([os.environ['ELASTIC_SEARCH_SERVER']])
elastic_index = os.environ['ELASTIC_SEARCH_INDEX']
global_path_url_download = os.environ['DOWNLOADS_ENDPOINT']
global_email_user = os.environ['MAIL_DOWNLOADS']
global_email_psw = os.environ['PASSWORD_DOWNLOADS']
global_server_email = 'smtp.gmail.com'
global_port_email = 587
global_email_to_list = ['']
temporal_file_prefix = "temp"
global_path_download = 'download'
destinatario="SiB Colombia"


rq = RQ()


keys = [
        "organizationName",
        "doi",
        "titleResource",
        "occurrenceID",
        "basisOfRecord",
        "collectionCode",
        "catalogNumber",
        "type",
        "licence",
        "rightsHolder",
        "collectionID",
        "recordedBy",
        "individualCount",
        "organismQuantity",
        "organismQuantityType",
        "sex",
        "lifeStage",
        "preparations",
        "disposition",
        "samplingProtocol",
        "eventDateOrg",
        "continent",
        "country",
        "stateProvince",
        "county",
        "locality",
        "minimumElevationInMeters",
        "maximumElevationInMeters",
        "minimumDepthInMeters",
        "maximumDepthInMeters",
        "decimalLatitude",
        "decimalLongitude",
        "geodeticDatum",
        "identifiedBy",
        "typeStatus",
        "dateIdentified",
        "scientificName",
        "kingdom",
        "phylum",
        "class",
        "order",
        "family",
        "genus",
        "specificEpithet",
        "infraSpecificEpithet",
        "taxonRank",
        "verbatimTaxonRank",
        "scientificNameAuthorship",
        "vernacularName"]

dataType={
        "organizationName":   object,
        "doi":   object,
        "titleResource":   object,
        "occurrenceID":   object,
        "basisOfRecord":   object,
        "collectionCode":   object,
        "catalogNumber":   object,
        "type":   object,
        "licence":  object,
        "rightsHolder":   object,
        "collectionID":   object,
        "recordedBy":   object,
        "individualCount": object,
        "organismQuantity":  object,
        "organismQuantityType":   object,
        "sex":   object,
        "lifeStage":  object,
        "preparations":   object,
        "disposition":   object,
        "samplingProtocol":   object,
        "eventDateOrg":   object,
        "stateProvince":   object,
        "county":   object,
        "locality":   object,
        "minimumElevationInMeters":  object,
        "maximumElevationInMeters":  object,
        "minimumDepthInMeters":  object,
        "maximumDepthInMeters":  object,
        "decimalLatitude":   object,
        "decimalLongitude":   object,
        "geodeticDatum":   object,
        "identifiedBy":   object,
        "typeStatus":  object,
        "dateIdentified":  object,
        "scientificName":   object,
        "kingdom":   object,
        "phylum":   object,
        "class":   object,
        "order":   object,
        "family":   object,
        "genus":   object,
        "specificEpithet":   object,
        "infraSpecificEpithet":  float,
        "taxonRank":   object,
        "verbatimTaxonRank":   object,
        "scientificNameAuthorship":   object,
        "vernacularName":   object}


def send_message(to, subject, body):
    # Función para el envio de la notificación por correo.
    try:
        session = smtplib.SMTP(global_server_email, global_port_email)
        session.ehlo()
        session.starttls()
        session.ehlo()
        session.login(global_email_user, global_email_psw)
        headers = [
            "From: " + destinatario,
            "Subject: " + subject,
            "To: " + ", ".join(to),
            "MIME-Version: 1.0",
            "Content-Type: text/html"]
        headers = "\r\n".join(headers)
        session.sendmail(
            destinatario,
            to,
            headers + "\r\n\r\n" + body)
        print("Message Send")
    except Exception as e:
        traceback.print_exc()
        print("\t Error al enviar el mensaje.", e)


def elastic_query(request):
    try:
        s = Search(using=es, index=elastic_index)
        s = build_query2(request, s)
        count = s.count()
        s = s[0:count]
        result_es = s
        flag_result = True
    except Exception as e:
        traceback.print_exc()
        print("\t ** Error elastic_query: ", e)
        result_es = None
        flag_result = False
    return flag_result, result_es, count


def create_zip(hash):
    # Función que genera el ZIP a partir del archivo que se
    # genero de la descarga.
    try:
        print('creating archive')
        file_csv = '{0}{1}{2}.csv'.format(global_path_download, os.sep, hash)
        file_zip = '{0}.zip'.format(hash)
        with zipfile.ZipFile(file_zip, mode='w') as zf:
            print('adding {0}'.format(file_csv))
            zf.write(file_csv)
        os.remove(file_csv)
        print('remove {0}'.format(file_csv))
        return True
    except Exception as e:
        traceback.print_exc()
        print("\t ** Error create_zip: ", e)
        return False, ""

def delete_temp_files():
    # Función que borra los archivos temporales. Esta función es llamada
    # una vez el procesamiento culmina.
    print("Borrando archivos temporales de la descarga ...")
    temps_files = glob.glob('{0}{1}{2}-*'.format(global_path_download,os.sep,temporal_file_prefix))
    try:
        for temp_file in temps_files:
            os.remove(temp_file)
    except Exception as e:
        traceback.print_exc()
        print("Error al borrar de disco los archivos temporales ...")


from redis import Redis
rc = Redis(os.environ['REDIS_SERVER'], os.environ['REDIS_SERVER_PORT'])

@rq.job(timeout=360000)
def generate_download_job(request, email, currentUrl):
    print("---------------------------#########################################--------------------------------")
    print(currentUrl)
    # Función que ejecuta el job asíncrono de la
    # descarga de un archivo de consulta.
    print("Start Job for {0}".format(email))
    print("Request : {0}".format(request))

    flag, result, count = elastic_query(request)

    print("Número Total de registros de la consulta : "+str(count))

    if not flag:
        return flag

    shardSize=10000000

    if count > 100000:
        shardSize = 100000

    query_download_name = generate_query_download_name(request)

    flag_zip=True
    name_zip=query_download_name+".zip"
    if (os.path.exists(name_zip)):
        print("El ZIP de la consulta "+name_zip+" ya se ha generado en una anterior consulta y se enviará al destino ...")
    else:
        if (generate_result_cvs(result, shardSize, '{0}{1}{2}'.format(global_path_download, os.sep, temporal_file_prefix))):
            file_name='{0}{1}{2}.csv'.format(global_path_download, os.sep, str(query_download_name))
            # Unir archivo temporales en un solo archivo
            if generate_download_file(file_name, shardSize):
                print("Se genero el archivo csv para la descarga ...")
                flag_zip = create_zip(query_download_name)
            else:
                print("No se pudo generar el archivo csv para para la descarga ...")
    subject_type = "Descarga - SiB Colombia"
    if flag_zip:
        url_download=global_path_url_download + name_zip
        currentUrlFormated=formatToAscii(currentUrl)
        message_email = "&#161;Hola " + email + "&#33;<br>\n" \
                                                " Muchas gracias por hacer uso de los canales de participaci&#243;n del SiB Colombia. <br>\n<br>\n" \
                                                " Tu consulta fue: <a href=\""+ currentUrlFormated + "\">"+currentUrlFormated+"</a><br>\n<br>\n"  \
                                                " Para descargar los datos consultados en el Portal de Datos, haz clic en el siguiente enlace: <br>\n<br>\n" \
                        + url_download + "<br>\n<br>\n" \
                                         "<span style ='color: black; font-family:arial; font-size: 1em; font-style: italic;'><span style ='color: black; font-family:arial; font-size: 1em; font-style: italic;font-weight: bold;'>Nota</span>: El archivo descargado se encuentra en formato .csv (valores separados por comas) y" \
                                         " codificaci&#243;n UTF8, ten presente esta informaci&#243;n si vas a visualizar el archivo con un procesador de texto. En el caso de Excel usa la opci&#243;n Importar " \
                                         "archivos de texto.</span><br>\n<br>\n" \
                                         "<span style ='color: black; font-family:arial; font-size: 1em;font-weight: bold; font-style: italic';> &#191;Quieres contarnos para qu&#233; servir&#225;n los datos&#63; </span> " \
                                         "Responder esta encuesta no te tomar&#225; m&#225;s de dos minutos -> https://forms.gle/S1vo53AU9LUSip4h9<br>\n<br>\n" \
                                         "<span style ='color: black; font-family:arial; font-size: 1em;'><span style ='color: black; font-family:arial; font-size: 1em ;font-weight: bold;'>Uso y atribuci&#243;n: <br>\n </span>" \
                                         "Los datos que se comparten a trav&#233;s del SiB Colombia provienen de diferentes entidades" \
                                         " nacionales e internacionales que aportan al conocimiento abierto." \
                                         " En el SiB Colombia los llamamos socios publicadores. Las condiciones de uso de esta informaci&#243;n las define cada socio publicador mediante licencias Creative Commons." \
                                         " Por eso es necesario leer y respetar las condiciones legales que aparecen asociadas a cada uno de los contenidos. Para consultarlas puedes ver el elemento " \
                                         "<span style ='color: black; font-family:arial; font-size: 1em;font-weight: bold; font-style: italic';>&#39;license&#39;</span> en " \
                                         "la descarga o puedes encontrar los detalles de citaci&#243;n siguiendo el elemento <span style ='color: black; font-family:arial; font-size: 1em;font-weight: bold; font-style: italic';>&#39;doi&#39;</span>.<br>\n<br>\n" \
                                         "&#8226; Siempre que utilices los datos o la informaci&#243;n disponible a trav&#233;s de los canales de participaci&#243;n del SiB Colombia, debes dar el cr&#233;dito a quien corresponda. " \
                                         "Si son varias fuentes, debes citarlas a todas.<br>\n" \
                                         "&#8226; No suprimas la informaci&#243;n del autor que el publicador haya asociado a los datos o a la informaci&#243;n.<br>\n" \
                                         "&#8226; Reconoce que los datos y la informaci&#243;n son suministrados con buena fe respecto de su calidad, validez y relevancia en el momento de su publicaci&#243;n, por eso al utilizarlos lo haces" \
                                         " bajo tu propia responsabilidad.<br>\n" \
                                         "&#8226; Ser cuidadoso en la citaci&#243;n es la forma de reconocer y agradecer el trabajo de los dem&#225;s, por eso es tan importante. Para m&#225;s informaci&#243;n sobre la pol&#237;tica de" \
                                         " acceso abierto del SiB Colombia, consulta:<br>\n [https://www.sibcolombia.net/acceso-abierto].<br>\n<br>\n" \
                                         "Gracias por utilizar nuestros canales de participaci&#243;n.<br>\n<br>\n" \
                                         "Cordialmente,<br>\n<br>\n" \
                                         "<span style ='color: black; font-family:arial; font-size: 1em;font-weight: bold;'>Equipo Coordinador del SiB Colombia</span><br>\n<br>\n" \
                                         "<span style ='color: black; font-family:arial; font-size: 1em; font-style: italic;'>Por favor no responda a este mensaje. Este mensaje se envi&#243; desde una cuenta de correo autom&#225;tica por una solicitud de descarga.</span><br><br>\n<br>\n</span>"


        send_message([email], subject_type, message_email)  # ,'sib@humbolt.com.co'
    else:
        message_email = "El archivo no se genero consulta al administrador del SIB Colombia.<br>\n"
        send_message([email], subject_type, message_email)  # ,'sib@humbolt.com.co'
    delete_temp_files()
    return True


def generate_query_download_name(request):
    # Función que genera el nombre de la descarga
    # a partir de los parametros y valores que contiene
    # la query de cosulta.
    print("-------------------REQUEST-----------------------------------")
    print(str(request))
    print("-------------------REQUEST-----------------------------------")
    query_donwload_name = ""
    phylums = request.getlist("phylum")
    query_donwload_name+="phylums = "
    for phylum in phylums:
            query_donwload_name += phylum + ","

    keyTypes = request.getlist("keyType")
    query_donwload_name += "keyTypes = "
    for keyType in keyTypes:
            query_donwload_name += keyType + ","

    scientificNames = request.getlist("scientificName")
    query_donwload_name += "scientificNames = "
    for scientificName in scientificNames:
        query_donwload_name += scientificName + ","

    kingdoms = request.getlist("kingdom")
    query_donwload_name += "kingdoms = "
    for kingdom in kingdoms:
        query_donwload_name += kingdom + ","

    classes = request.getlist("class")
    query_donwload_name += "classes = "
    for classTax in classes:
        query_donwload_name += classTax + ","

    orders = request.getlist("order")
    query_donwload_name += "orders = "
    for order in orders:
        query_donwload_name += order + ","


    families = request.getlist("family")
    query_donwload_name += "families = "
    for family in families:
        query_donwload_name += family + ","

    genusList = request.getlist("genus")
    query_donwload_name += "genus = "
    for genus in genusList:
        query_donwload_name += genus + ","

    specificEpithets = request.getlist("specificEpithet")
    query_donwload_name += "specificEpithets = "
    for specificEpithet in specificEpithets:
        query_donwload_name += specificEpithet + ","

    infraSpecificEpithets = request.getlist("infraSpecificEpithet")
    query_donwload_name += "infraSpecificEpithets = "
    for infraSpecificEpithet in infraSpecificEpithets:
        query_donwload_name += infraSpecificEpithet + ","


    countries = request.getlist("country")
    query_donwload_name += "countries = "
    for country in countries:
        query_donwload_name += country + ","

    statesProvinces = request.getlist("stateProvince")
    query_donwload_name += "statesProvinces = "
    for stateProvince in statesProvinces:
        query_donwload_name += stateProvince + ","

    counties = request.getlist("county")
    query_donwload_name += "counties = "
    for county in counties:
        query_donwload_name += county + ","

    localities = request.getlist("locality")
    query_donwload_name += "localities = "
    for locality in localities:
        query_donwload_name += locality + ","

    taxonRanks = request.getlist("taxonRank")
    query_donwload_name += "taxonRanks = "
    for taxonRank in taxonRanks:
        query_donwload_name += taxonRank + ","

    habitats = request.getlist("habitat")
    query_donwload_name += "habitats = "
    for habitat in habitats:
        query_donwload_name += habitat + ","

    basisOfRecords = request.getlist("basisOfRecord")
    query_donwload_name += "basisOfRecords = "
    for basisOfRecord in basisOfRecords:
        query_donwload_name += basisOfRecord + ","

    recorders = request.getlist("recordedBy")
    query_donwload_name += "recorders = "
    for recorder in recorders:
        query_donwload_name += recorder + ","

    occurrenceIDs = request.getlist("occurrenceID")
    query_donwload_name += "occurrenceIDs = "
    for occurrenceID in occurrenceIDs:
        query_donwload_name += occurrenceID + ","

    organizationIds = request.getlist("organizationId")
    query_donwload_name += "organizationIds = "
    for organizationId in organizationIds:
        query_donwload_name += organizationId + ","

    projects = request.getlist("projectSib")
    query_donwload_name += "projects = "
    for project in projects:
        query_donwload_name += project + ","

    gbifIds = request.getlist("gbifId")
    query_donwload_name += "gbifIds = "
    for gbifId in gbifIds:
        query_donwload_name += gbifId + ","

    mediaTypes = request.getlist("mediaType")
    query_donwload_name += "mediaTypes = "
    for mediaType in mediaTypes:
        query_donwload_name += mediaType + ","

    licenses = request.getlist("license")
    query_donwload_name += "licenses = "
    for license in licenses:
        query_donwload_name += license + ","

    print("-----------------------------------query")
    print(query_donwload_name)
    hash_object = hasher.md5(query_donwload_name.encode('utf-8'))
    query_donwload_name=hash_object.hexdigest()
    return query_donwload_name


def generate_result_cvs(result, shardSize, temporalFilePrefix):
    # Función que escribe los resultados de la consulta en
    # archivos temporales csv que posteriormente seran leidos
    # para ensamblar el archivo final.
    shardNumber = 0
    resultRowNumber = 0
    writer = None
    file = None
    source_csv=[]
    for item in result.scan():
        try:
            if(resultRowNumber < shardSize):
                source_csv.append(item)
                resultRowNumber += 1
            else:
                print("Generando shard # " + str(shardNumber))
                file = open('{0}-{1}.csv'.format(temporalFilePrefix, str(shardNumber)), 'w')
                writer = csv.DictWriter(file, fieldnames=keys, extrasaction='ignore')
                writer.writeheader()
                for row in source_csv:
                    writer.writerow(row.to_dict())
                file.close()
                source_csv = []
                source_csv.append(item)
                resultRowNumber = 1
                shardNumber += 1
        except IOError:
            print("I/O error")
            traceback.print_exc()
            return False
        except Exception as e:
            print("Error desconocido al tratar de generar los csv temporales de la consulta.")
            traceback.print_exc()
            return False
    try:
        file = open('{0}-{1}.csv'.format(temporalFilePrefix, str(shardNumber)), 'w')
        writer = csv.DictWriter(file, fieldnames=keys, extrasaction='ignore')
        writer.writeheader()
        for row in source_csv:
            writer.writerow(row.to_dict())
        file.close()
    except Exception as e:
        traceback.print_exc()
        print("Error desconocido al tratar de generar los csv temporales de la consulta.")
        return False
    source_csv = []
    resultRowNumber = 0
    shardNumber = 0
    return True

def read_csv(fileName, chunkSize):
    # Función que realiza la lectura parcial del csv en memoria.
    # El parametro "dtype" es opcional, sin embargo es funcamental
    # para la ejecución en entornos donde la memoria disponible
    # es baja.
    try:
        return pd.read_csv(fileName, dtype=dataType, index_col=0, chunksize=chunkSize)
    except Exception as e:
        print("Error al leer el csv con nombre : "+ fileName)
        traceback.print_exc()
        raise e


def concat_row(row):
    # Función para la contatenación de registros en el
    # dataframe.
    try:
        return pd.concat(row, axis=0, sort=False)
    except Exception as e:
        print("Error al concatenar la columna : "+ row)
        traceback.print_exc()
        raise e

def write_csv(outputFileName, df):
    # Esta función se encarga de escribir prograsivamente un dataframe en
    # un archivo de salida csv prograsivamente. Si el archivo aun no existe
    # tomara el dataframe como el header del archivo de salida, de lo
    # contrario tomará el dataframe como una columna de datos.
    try:
        if os.path.exists(outputFileName):
            mode = 'a'
            header = False
        else:
            mode = 'w'
            header = True
        df.to_csv(outputFileName, mode=mode, header=header)
    except Exception as e:
        print("Error al escribir en el archivo : " + outputFileName)
        traceback.print_exc()
        raise e

def merge_files(temporal_files,outputFileName, shard_size):
    # Función para unir los csv temorales para armar el archivo defiitivo de la desacarga.
    # Para este caso se utiliza el parámetro "chunkSize" el cual define la cantidad máxima
    # de registros a trabajar en memoria. Este parámetro es util cuando se procesan archivos
    # de gran tamaño. Solo 1 "chunk" o partición se concatena a la vez y se escribe en el
    # archivo de destino.
    try:
        for row in zip(*[read_csv(fileName, chunkSize=shard_size) for fileName in temporal_files]):
            write_csv(outputFileName, concat_row(row))
    except Exception as e:
        print("Error al unir los archivos temporales de la descarga.")
        traceback.print_exc()
        raise e

from natsort import natsorted

def generate_download_file(file_name, shard_size):
    # Función que orquesta la lógica para generar el archivo de descarga.
    # Lo primero que hace esta función es ordenar los archivos temporales
    # y enviarselos a la función "merge_files()" la cual se encarga de
    # encarga de progresivamente ensamblar el archivo "
    try:
        files_names='{0}{1}{2}-*'.format(global_path_download, os.sep, temporal_file_prefix)
        temp_files = natsorted(glob.glob(files_names))
        merge_files(temp_files, file_name, shard_size)
        return True
    except Exception as e:
        print("Error al unir los archivos temporales de la descarga.")
        traceback.print_exc()
        return False



if __name__ == "__main__":
    correoPruebas = 'sib@humbolt.com.co'
    job = generate_download_job.delay(correoPruebas, {
        'query': {'query_string': {'query': '(gbifId:"11f5ca15-8de9-4499-9fd2-368b62ea45cb")'}}})
    print("job.result :", job.result)
    print("job.result :", job.result)
    job2 = generate_download_job.delay(correoPruebas, {
        'query': {'query_string': {'query': '(gbifId:"cce1c58a-880b-4b93-acc7-98c9b85900cd")'}}})
    print("job2.result :", job2.result)
    print("job2.result :", job2.result)
    job3 = generate_download_job.delay(correoPruebas, {
        'query': {'query_string': {'query': '(gbifId:"e6600dde-9172-4875-9125-8b7457eb0c6b")'}}})
    print("job3.result :", job3.result)
    print("job3.result :", job3.result)