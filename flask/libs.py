
from elasticsearch_dsl import A, Q
import re as regex

def build_query(request, extra=None):
    condicion = []
    if extra is not None:
        condicion.append(extra)
    #    pass
    for a in request.args:
        if a != "agg" and a != "page" and a != "valor" and a != "campo":
          #condicion.append ({ "term": { a: request.args[a] } })
          lista = []
          for x in request.args.getlist(a):
              if a!="undefined" and a!="":
                  b = a.replace("?", "")
                  if b!="":
                    lista.append(b+":\""+x+"\"")
          if len(lista)>0:
            condicion.append("("+" OR ".join(lista)+")")
    return " AND ".join(condicion)


def build_query2(request_args, s):

    for a in request_args:
        if a != "agg" and a != "page" and a != "valor" and a != "campo" and a != "limit" and a != "email" and a != "currentUrl":
          #condicion.append ({ "term": { a: request_args[a] } })
          q = None
          for x in request_args.getlist(a):
              if a!="undefined" and a!="":
                  b = a.replace("?", "")
                  if b!="":
                    #lista.append(Q('match', a=x))
                    if q!=None:
                        q = q | Q("multi_match", query=x, fields=[b])
                    else:
                        q = Q("multi_match", query=x, fields=[b])
                  
          if q!=None:
              print(Q('bool', must=q))
              s=s.query(Q('bool', must=q))
    return s


def count_build_query(request):
    condicion = 0
    for a in request.args:
        if a != "agg" and a != "page" and a != "valor" and a != "campo":
          condicion = condicion + 1
    return condicion

def formatToAscii(string):
    return string.replace("À","&#192;").replace("Á","&#193;").\
                    replace("È","&#200;").replace("É","&#201;").\
                    replace("Ì","&#204;").replace("Í","&#205;").\
                    replace("Ò", "&#210;").replace("Ó", "&#211;").\
                    replace("Ù","&#217;").replace("Ú","&#218;").\
                    replace("à","&#224;").replace("á","&#225;").\
                    replace("è", "&#232;").replace("é", "&#233;").\
                    replace("ì", "&#236;").replace("í", "&#237;").\
                    replace("ò", "&#242;").replace("ó", "&#243;").\
                    replace("ù", "&#249;").replace("ú", "&#250;")

def formatUrlSearch(request):
    requestStr=str(request)
    currentUrlSearch = regex.search('(.*)currentUrl=http:%2F%2F(.*)%2Fsearch%3F(.*)\' \[GET\]', requestStr, regex.IGNORECASE)
    currentUrl=""
    if currentUrlSearch:
        url = currentUrlSearch.group(2)
        query = currentUrlSearch.group(3)
        currentUrl="http://"+url+"/search?"+query
    return currentUrl