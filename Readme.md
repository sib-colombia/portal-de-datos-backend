# PORTAL-DE-DATOS-BACKEND

API que se encarga de realizar las consultas para los diferentes servicios que
requieren los 3 canales de participación del SiB Colombia: Portal de Datos,
Colecciones en Linea y Listas de Especies.

El API Datos tiene por 2 componentes:

![](https://docutopia.tupale.co/uploads/upload_8611ff5b730aa636a51469cc91e3aada.png)




 * **API Datos Búsqueda**
>API que se encarga de realizar las consultas a elasticsearch para las búsquedas
y el motor de descargas.

 * **API Datos Consulta** 
>API para la consulta de datos específicos sobre mongo. Esta API realiza
consultas de Registros, Recursos y Publicadores.


## Tecnologías

Esta API está desarrollada sobre Python3.7.2 [\[1\]](https://www.python.org/downloads/release/python-372/) utilizando el framework Flask [\[2\]](http://flask.pocoo.org/)

El API Datos Búsqueda utiliza para las consultas elasticsearch_dsl [\[3\]](https://github.com/elastic/elasticsearch-dsl-py/) ,para el
proceso de descargas en el control de Redis [\[4\]](https://redis.io/) sobre Flask_rq2 [\[2\]](http://https://github.com/rq/Flask-RQ2/) y para la generación de archivos de descargas se emplea la libreria pandas [\[6\]](https://pandas.pydata.org)

 * [1] https://www.python.org/downloads/release/python-372
 * [2] http://flask.pocoo.org
 * [3] https://github.com/elastic/elasticsearch-dsl-py
 * [4] https://redis.io
 * [5] https://github.com/rq/Flask-RQ2
 * [6] https://pandas.pydata.org

El API Datos Consulta utiliza para las consultas pymongo [\[6\]](https://github.com/elastic/elasticsearch-dsl-py/) y para el
proceso de descargas en el control de Redis [\[7\]](https://redis.io/) sobre Flask_rq2 [\[8\]](https://github.com/rq/Flask-RQ2)

 * [6] https://api.mongodb.com/python/current/
 * [7] https://redis.io/
 * [8] https://github.com/rq/Flask-RQ2


## Instalación de ambiente

* Instale python y sus ambientes virtuales tal como de describe en el tutorial [Instalación Python 3.7.2 para despliegues en producción en Debian 9](https://docutopia.tupale.co/-ie3mEIJTcqLQCQVUy1Lrw?both)

* Iniciar el ambiente virtual de python 3.7 con el comando:
```shell
source ../ruta_ambiente_virtual/bin/activate
```

* Una vez activado el ambiente virtual de python 3.7. Instalar las dependencias con el paquete pip, el cual es el manejador de paquetes más popular para python:

```shell=
## Ejecución dentro del ambiente virtual de python 3.7
python -m pip install elasticsearch_dsl elasticsearch flask gunicorn rq unicodecsv flask_cors flask_rq2 pymongo pandas natsort
```
Una opcion es mas sencilla es ejecutar pip con base a las dependencias que se encuentran en el archivo requirements.txt. Para generar este archivo, parese sobre la raiz del proyecto y  ejecute el comando **pip freeze**: 

```shell=
pip freeze > requirements.txt
```

Para instalar las dependencias con base a este archivo ejecute dentro del ambiente virtual de python que este usando:

```shell=
python -m pip requirements.txt
```

* Instale redis-server en la máquina en donde se ejecutaran el worker de descargas de archivos:

```shell=
sudo apt install redis-server
```


## Herramientas

 * redis-comander

Herramienta web para la administración y visualización de los datos en redis

https://github.com/joeferner/redis-commander


# Instalación

**1.** Crear carpeta **/opt/apps/** la cual es la dirección definida *(es opcional)* en donde se desplegarán los API´s de búsquedas y de consultas en los servidores. Es útil mantener estas rutas pues son las establecidas en las máquina de desarrollo de los portales:    

```shell=
sudo mkdir -p /opt/apps/
chown usuario_despliegue.usuario_despliegue /opt/apps/
```

**2.** Clonar el proyecto desde el repositorio desde la ruta estándar **/opt/apps/**:

```shell=
cd /opt/apps/
git clone https://gitlab.com/sib-colombia/portal-de-datos-backend
```

**3.** Crear carpeta de logs y asignar al usuario prodserv como propietario de la misma. La ruta estándar será **/var/log/apps** *(es opcional)* y asignamos como dueño del directorio al usuario con el cúal se harán los despliegues en producción:
```shell=
sudo mkdir -p /var/log/apps/portal-de-datos/
chown usuario_despliegue.usuario_despliegue -R /var/log/apps/
```

**4.** Crear en el servidor correspondiente el log del API de búsquedas:
```shell=
touch /var/log/apps/portal-de-datos/apiDatosBusquedas.log
```

**5.** Crear en el servidor correspondiente el log del API de consultas:
```shell=
touch /var/log/apps/portal-de-datos/apiDatosConsultas.log
```

**6.** Entrar al directorio del proyecto **portal-de-datos-backend** el cúal tendrá la siguiente estructura:

```bash
# Estructura de la carpeta generado por el indice. 
+--portal-de-datos-backend
|    +-- flask
|    +-- test 
|    +-- Readme.md
```
<br/> 

**7.** Se deberán configurar las variables de entorno, las cuales contendrán datos tales como la dirección de los servidores de **redis**, **elastic-search** y **mongodb**, puerto y credenciales de conexión. Para configurar las variables de entrno se modificará el archivo **bashrc** del usuario con el cúal se haga el despliegue. Para tal fin se deben ejecutar los siguientes comandos:

</br>

* Edite el archivo **bashrc** del usuario con el que se hará el despliegue:
    
```shell=
##editar archivo bashrc
vim ~/.bashrc
```

</br>

* Agregue las siguientes variables de entorno al final del archivo **bashrc**, las cuales quedarán en el perfil de usuario y serán las que se tomen desde la aplicación:

</br>

**Para Api Datos Consultas:**
```shell=
##Ejemplo configuración variables de entorno Api Datos Consultas
.
.
.
API_CONSULTAS_PORT=6002
MONGO_SERVER="mongodb://192.168.11.40:277"
MONGO_DATA_BASE="db-sib"
export API_CONSULTAS_PORT
export MONGO_SERVER
export MONGO_DATA_BASE
.
.
.
```

</br>

**Para Api Datos Búsquedas:** 

El despliegue actual del API de búsquedas se muestra en la siguiente imagen:

![](https://docutopia.tupale.co/uploads/upload_21f228e963bef402ef66e22ab3778dbe.png)

Como se puede obervar, hay dos instancias del API de búsquedas; una que se encarga de las descargas y la otra que se encarga de las propias búsquedas. En cada uno de los servidores en donde se desplieguen las instancias se deben stear la variables de entorno que son tomadas por las aplicación tal como se muestra:

```shell=
##Ejemplo configuración variables de entorno Api Datos Búsquedas
.
.
.
API_BUSQUEDAS_PORT="6001"
ELASTIC_SEARCH_SERVER="elastic.sibcolombia.co"
ELASTIC_SEARCH_INDEX="db-sib"
REDIS_SERVER="redis.sibcolombia.co"
REDIS_SERVER_PORT=8521
DOWNLOADS_ENDPOINT="http://descargas.sibcolombia.co/api/download/"
MAIL_DOWNLOADS="descargas@sibcolombia.co"
PASSWORD_DOWNLOADS="contrasena_correo"
export API_BUSQUEDAS_PORT
export ELASTIC_SEARCH_SERVER
export ELASTIC_SEARCH_INDEX
export REDIS_SERVER
export REDIS_SERVER_PORT
export DOWNLOADS_ENDPOINT
export MAIL_DOWNLOADS
export PASSWORD_DOWNLOADS
.
.
.
```

* Actualice las variables de entorno de usuario con el comando **source** sobre el archivo **bashrc**:
```shell=
##Actualizar bashrc
source ~/.bashrc

```

</br>

**8.** Entrar a la carpeta **flask** 

# Despliegue en desarrollo:

Para la ejecución de los API´s en desarrollo ingrese al directorio del proyecto:

Para Api Datos Búsquedas: 
```shell=
##Api Datos Búsquedas
cd flask/
python3 apiDatosBusquedas.py
```

Y para Api Datos Consultas
```shell=
##Api Datos Consultas
cd flask/
python3 apiDatosConsultas.py
```

Para terminar la ejecución del backend se presiona ctrl+c.

Actualmente en el servidor se pruebas se encuentra una instrucción que automatiza estos pasos y ejecuta las AṔIs y el frontend del portal de datos dejandolos como procesos de background. Para desplegarlos siga los siguientes pasos.

1. Ingrese al servidor de pruebas.
2. Escriba en la consonla `vpy37`, esto activará el ambiente de desarrollo en python 3.7. **Sin esta instrucción los servicios iniciarán con errores de despliegue.**
2. Escriba `load_portals`, esto activará las APIs y el portal de datos. La salida que va a ver es similar a esta:

```shell
>>>> In-memory PM2 is out-of-date, do:
>>>> $ pm2 update
In memory PM2 version: 2.10.4
Local PM2 version: 3.4.0

[PM2] Starting /usr/bin/npm in fork_mode (1 instance)
[PM2] Done.
┌──────────────────────────┬────┬─────────┬──────┬───────┬────────┬─────────┬────────┬─────┬──────────┬──────┬──────────┐
│ App name                 │ id │ version │ mode │ pid   │ status │ restart │ uptime │ cpu │ mem      │ user │ watching │
├──────────────────────────┼────┼─────────┼──────┼───────┼────────┼─────────┼────────┼─────┼──────────┼──────┼──────────┤
│ apiDatosBusquedas        │ 0  │ N/A     │ fork │ 29557 │ online │ 0       │ 2s     │ 0%  │ 2.8 MB   │ sib  │ disabled │
│ apiDatosConsultas        │ 1  │ N/A     │ fork │ 29589 │ online │ 0       │ 1s     │ 0%  │ 2.9 MB   │ sib  │ disabled │
│ portal-de-datos-frontend │ 2  │ N/A     │ fork │ 29632 │ online │ 0       │ 0s     │ 0%  │ 4.0 KB   │ sib  │ disabled │
└──────────────────────────┴────┴─────────┴──────┴───────┴────────┴─────────┴────────┴─────┴──────────┴──────┴──────────┘
 Use `pm2 show <id|name>` to get more details about an app
```

`load_portals` se encuentra en la ruta `/usr/local/bin` y contiene la siguiente estructura:

```shell
#!/bin/bash
echo "Cargando portales" 
cd /opt/apps/portal-de-datos/portal-de-datos-backend/flask
pm2 --name="apiDatosBusquedas" start apibusquedas.sh
pm2 --name="apiDatosConsultas" start apiconsultas.sh
cd /opt/apps/portal-de-datos/portal-de-datos-frontend
pm2 --name="portal-de-datos-frontend" start npm -- start
exec bash
sh
```

# Despliegue en producción:

En los servidores de API Búsquedas y API Consultas se encuentra un script en bash para ejecutar el comando gunicorn con PM2. El script se encuentra en la carpeta `/portal-de-datos-backend/flask` con el nombre apiBusquedas.sh y apiCOnsultas.sh.en cada uno en el servidor correspondiente. La instrucción que ejecuta estos scripts es `load_api`. En este momento cada API está en un servidor diferente por lo que se debera ingresar a cada uno y realizar los siguientes pasos.

## Despliegue del API como proceso de background.

Instrucciones:

1. Ingrese al servidore del API.
2. Liste los procesos activos con el comando `pm2 list`.
3. Si el API está en ejecución, aparecerá en la lista de procesos activos ejecutados con pm2.
4. Si requiere volver a cargar el api puede hacerlo con `pm2 restart` y si desea deterner el servicio lo puede hacer con `pm2 delete X`, donde x es el id del proceso que aparece en el lista. **Tenga en cuenta que si detiene el proceso el servicio del api estará suspendido del portal en producción hasta que se vuelva a ejecutar, así que procure que sea el menor tiempo posible y/o en jornadas de pocas consultas.** 
5. Para volver a ejecutar el API, escriba en la consonla `vpy37`, esto activará el ambiente de desarrollo en python 3.7.
6. Luego escriba el comando `load_api`. Se cargará el proceso y quedará ejecutándose hasta que se detenga manualmente o por otra circunstancia. Un ejemplo de la salida cuando se ejecuta el script es:

```shell
>>>> In-memory PM2 is out-of-date, do:
>>>> $ pm2 update
In memory PM2 version: 2.10.4
Local PM2 version: 3.4.0

[PM2] Applying action deleteProcessId on app [2](ids: 2)
[PM2] [portal-de-datos-frontend](2) ✓
┌───────────────────┬────┬─────────┬──────┬───────┬────────┬─────────┬────────┬─────┬──────────┬──────┬──────────┐
│ App name          │ id │ version │ mode │ pid   │ status │ restart │ uptime │ cpu │ mem      │ user │ watching │
├───────────────────┼────┼─────────┼──────┼───────┼────────┼─────────┼────────┼─────┼──────────┼──────┼──────────┤
│ apiDatosBusquedas │ 0  │ N/A     │ fork │ 13827 │ online │ 0       │ 44h    │ 0%  │ 2.8 MB   │ sib  │ disabled │
│ apiDatosConsultas │ 1  │ N/A     │ fork │ 13857 │ online │ 0       │ 44h    │ 0%  │ 2.8 MB   │ sib  │ disabled │
└───────────────────┴────┴─────────┴──────┴───────┴────────┴─────────┴────────┴─────┴──────────┴──────┴──────────┘
 Use `pm2 show <id|name>` to get more details about an app

```

La instrucción que despliega las API a producción se encuentra en la ruta `/usr/local/bin` con el nombre `load_portals` y contiene la siguiente estructura:

```shell
echo "Cargando Api de búsquedas..." 
cd /opt/apps/portal-de-datos-backend/flask
pm2 --name="nombredelproceso" start scriptdelapi.sh
```


## Otras opciones para ejecutar el prioceso.

En caso de que el despligue con pm2 presente algún problema y no se pueda ejecutar, se puede dejar ejecutando el servicio en background con otros comandos como estos:

Para correr la aplicación como demonio se pueden usar los paquetes Byobu o nohup
 * https://help.ubuntu.com/community/Byobu


```shell=
byobu
```

Esto despliega una sesión de bash que se queda ejecutando en background así uno
se desconecte del servidor. En esta terminal se puede ejecutar el comando y salir.

Importante, para desconectarse de la sesión se presiona la tecla f6


 * https://en.wikipedia.org/wiki/Nohup

```shell=
nohup comando &
```

Esto despliega el comando en la bash y este se queda ejecutando en background
así uno se desconecte del servidor. Para deterner este comando se puede hacer
con el PID, ```kill -9 PID```.

# Ejecución del worker de descargas

Para las descargas que se programan mediante los servicios del API Datos
Consulta se debe ejecutar un worker de Redis sobre la carpeta /portal-de-datos-backend/flask/. Esto se hace con el comando:
```bash
rq worker
```

Este comando ejecutara la clase download.py, que es la que implementa la lógica del worker de descargas. Se debe tener una salida similar a esta cuando está trabajando:
```bash
15:04:11 RQ worker 'rq:worker:dellicioso.18057' started, version 0.13.0
15:04:11 *** Listening on default...
15:04:11 Cleaning registries for queue: default
15:04:30 default: download.create_download_job(ImmutableMultiDict([('basisOfRecord', 'livingspecimen'), ('email', 'pepe@pepe.com')]), 'pepe@pepe.com') (4016ff96-e839-4782-bc97-3306f07cdee2)
/usr/lib/python3/dist-packages/requests/__init__.py:80: RequestsDependencyWarning: urllib3 (1.24.1) or chardet (3.0.4) doesn't match a supported version!'
  RequestsDependencyWarning)
Start Job for pepe@pepe.com
Bool(must=[MultiMatch(fields=['basisOfRecord'], query='livingspecimen')])
Resultados de elastic:  <elasticsearch_dsl.search.Search object at 0x7f8020d91d68>
True <elasticsearch_dsl.search.Search object at 0x7f8020d91d68>
el scan es:  <Hit(sib-final2/register/cede7952-1b31-49a9-b3f2-818ec33f45f3): {'projectSib': '', 'resourceLogoUrl': 'https://s3.amazonaws'....}>
el scan es:  <Hit(sib-final2/register/1b4d2c13-93e0-4d44-bb77-f9b21461da2b): {'projectSib': '', 'resourceLogoUrl': 'https://s3.amazonaws'....}>
...

creating archive
adding download/2aa2729556ccb49672b2815074f4ce38c19be9f1.csv
remove download/2aa2729556ccb49672b2815074f4ce38c19be9f1.csv.txt
Message Send
15:06:04 default: Job OK (bdb3061e-1786-40cd-8400-b46dff986786)
15:06:04 Result is kept for 500 seconds


```

Tener precauciones con la libreria download de python, pues se puede presentar error al ejecutar el worker ya que esta libreria podria sobre-escribir la libreria "download" que implementa la lógica del worker. Para asegurar de que esto no pase ejecutar el siguinete comando: 

```shell=
pip uninstall download
```

![Redis Commander](https://gitlab.com/sib-colombia/portal-de-datos-backend/wikis/uploads/fdf584221c1a977c52f6f8e3dbdb3318/Captura_de_pantalla_de_2019-01-29_17-50-22.png "Redis Commander")

# Ejecución en servidor 2019-01-29


```bash
# Acceso al servidor
debian@vps189922:~$ ssh usuario@servidor
# Cambiar al usuario administrador del sistema
debian@vps189922:~$ su debian
# Vamos a la carpeta home del usuario
debian@vps189922:/root$ cd
# Entramos en la carpeta myproject donde está el proyecto
debian@vps189922:~$ cd myproject/
# Ejecutamos byobu para dejar el proyecto en background
debian@vps189922:~/myproject$ byobu

# Ejecutamos el comando gunicorn para correr el servicio con multiples hilos
debian@vps189922:~/myproject$ gunicorn -w 4 --bind 0.0.0.0:8000 wsgi
[2019-01-29 17:38:17 -0500] [15965] [INFO] Starting gunicorn 19.8.1
[2019-01-29 17:38:17 -0500] [15965] [INFO] Listening at: http://0.0.0.0:8000 (15965)
[2019-01-29 17:38:17 -0500] [15965] [INFO] Using worker: sync
[2019-01-29 17:38:17 -0500] [15968] [INFO] Booting worker with pid: 15968
[2019-01-29 17:38:17 -0500] [15970] [INFO] Booting worker with pid: 15970
[2019-01-29 17:38:17 -0500] [15971] [INFO] Booting worker with pid: 15971
[2019-01-29 17:38:17 -0500] [15972] [INFO] Booting worker with pid: 15972


# Para salir de este comando se ejecuta f6

```










