#!/usr/bin/python
# coding: utf-8
import requests, json, ast
from datetime import datetime
from elasticsearch import Elasticsearch
es = Elasticsearch()

## #########################################consultas###################
## #########################################consultas###################

consulta1={
  "size":"0",
  "aggs" : {
    "familias" : {
      "terms" : { "field" : "class" }
    }
  }
}

consulta22={
  "query": {
    "bool": {
      "filter": [
        { "term": { "class": "Aves"   }},
        { "term": { "family": "Falconidae" }}
      ]
    }
  },
  "aggs": {
    "models": {
      "terms": { "field": "genus" }
    }
  }
}

consulta333={
  "aggs": {
    "models": {
      "terms": { "field": "genus" }                      
    }
  }
}


consulta3={
    "match":{
        "field": "genus" }
  }

consulta33={
  "aggs": {
    "models": {
      "terms": { "field": "genus" }
    }
  }
}

consulta9={
  "query": {
      "match":
          { "genus" : "falco" }
      }
  }

consulta2={
  "size":"0",
  "aggs" : {
    "clases" : {
      "terms" : { "field" : "class" }
    },
    "familias" : {
      "terms" : { "field" : "phylum" }
    }
  }
}

consultaa={'query': {'match': {'phylum': 'Chordata'}}}
# consulta000 = {'query': {'match': {"field": "genus"}}}


## #########################################consultas###################
## #########################################consultas###################

consulta_def=consulta1

am=es.search(index="humboldt_2", body=consulta_def)

print (type(am))
varr = (json.dumps(am))
varr3=json.loads(varr)
print ("=====================")
print (varr)
print ("=========keys============")
print (varr3.keys())

diccionarios=[]
numerodatos=[]

print ("=========hits============")
campos=varr3["hits"]["hits"]
for i in campos: #.keys())
    jsons = ascii(i)
    diccionarios.append(ast.literal_eval(jsons))
    print ("$$$$$$$$$$$$$$$$$$$$$")

print (len(diccionarios))